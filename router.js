const router = require('express').Router()
const auth = require('./controllers/authController')
const restrict = require('./middlewares/restrict')
const redirectOnAuthenticated = require('./middlewares/redirectOnAuthenticated')

// Homepage
router.get('/', restrict, (req, res) => res.render('index'))

// Register Page
router.get('/register', redirectOnAuthenticated, (req, res) => res.render('register'))
router.post('/register', redirectOnAuthenticated, auth.register)
router.get('/login', redirectOnAuthenticated, (req, res) => res.render('login'))
router.post('/login', redirectOnAuthenticated, auth.login)
   
router.get('/whoami', restrict, auth.whoami)

module.exports = router;